#include "stm32f10x.h"
#include "freertos.h"
#include "task.h"


#define START_STK_SIZE  			100											/* 任务堆栈大小 					*/
#define START_TASK_PRIO   		1												/* 任务优先级 						*/
void start_task(void * prama);
TaskHandle_t startTaskHandle;


void LED_Task(void * prama);
TaskHandle_t LEDTaskHandle;

int main(void)
{

	xTaskCreate((TaskFunction_t )start_task,            /* 任务函数 							*/
							(const char*    )"start_task",          /* 任务名称 							*/
							(uint16_t       )START_STK_SIZE,        /* 任务堆栈大小 					*/
							(void*          )NULL,                  /* 传入给任务函数的参数 	*/
							(UBaseType_t    )START_TASK_PRIO,       /* 任务优先级 						*/
							(TaskHandle_t*  )&startTaskHandle);   	/* 任务句柄 							*/
	
	/* 
	* 启动freeRTOS,开启任务调度器
	*/
	vTaskStartScheduler();

  /* Infinite loop */
  while (1)
  {
		
  }

}
void start_task(void * prama) {

}
